const express = require('express');
const rateLimit = require('express-rate-limit');

const app = express();

// Apply rate limit to all requests: 5 requests per minute
app.use('/greet', rateLimit({
  windowMs: 60 * 1000, // 1 minute
  max: 5
}));

app.get('/greet', (req, res) => {
  res.json({ message: 'Hello, World, I\'m rate limited! :)' });
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
