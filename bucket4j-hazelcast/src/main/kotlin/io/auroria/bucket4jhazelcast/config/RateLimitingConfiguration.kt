package io.auroria.bucket4jhazelcast.config

import com.hazelcast.core.Hazelcast
import com.hazelcast.core.HazelcastInstance
import com.hazelcast.map.IMap
import io.github.bucket4j.Bandwidth
import io.github.bucket4j.Bucket
import io.github.bucket4j.BucketConfiguration
import io.github.bucket4j.grid.hazelcast.HazelcastProxyManager
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.time.Duration

@Configuration
class RateLimitingConfiguration {
    @Bean
    fun hazelcastInstance() = Hazelcast.newHazelcastInstance()

    @Bean
    fun rateLimitingMap(hazelcastInstance: HazelcastInstance): IMap<String, ByteArray> =
        hazelcastInstance.getMap("rate-limiters")

    @Bean
    fun hazelcastProxyManager(rateLimitingMap: IMap<String, ByteArray>) =
        HazelcastProxyManager(rateLimitingMap)

    @Bean
    @Qualifier("greeting-service-limiter")
    fun rateLimitingBucket(proxyManager: HazelcastProxyManager<String>): Bucket {
        val configuration = BucketConfiguration.builder()
            .addLimit(Bandwidth.simple(4, Duration.ofMinutes(1)))
            .build()

        return proxyManager.builder()
            .build("greeting-service-limit", configuration)
    }
}