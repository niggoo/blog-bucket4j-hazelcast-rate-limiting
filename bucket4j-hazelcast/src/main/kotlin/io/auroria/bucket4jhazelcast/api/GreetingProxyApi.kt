package io.auroria.bucket4jhazelcast.api

import io.auroria.bucket4jhazelcast.client.GreetingClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class GreetingProxyApi(private val greetingClient: GreetingClient) {
    @GetMapping("proxy-greet")
    fun rateLimitingCall() = greetingClient.greet()
}