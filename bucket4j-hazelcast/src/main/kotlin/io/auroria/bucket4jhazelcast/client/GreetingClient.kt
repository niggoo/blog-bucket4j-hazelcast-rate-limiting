package io.auroria.bucket4jhazelcast.client

import io.github.bucket4j.Bucket
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
class GreetingClient(
    private val restTemplate: RestTemplate,
    @Qualifier("greeting-service-limiter") private val bucket: Bucket
) {
    fun greet(): GreetingDTO {
        bucket.asBlocking().consume(1)
        return restTemplate.getForObject("http://localhost:3000/greet", GreetingDTO::class.java)!!
    }
}

data class GreetingDTO(
    val message: String
)
